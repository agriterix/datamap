
function unpack(rows, key) {
    return rows.map(function (row) { return row[key]; });
}

Date.prototype.addDays = function (days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

const r = window.crypto.getRandomValues(new Uint32Array(1));
let randomColor = "#" + ((1 << 24) * r | 0).toString(16);

d3.csv("data/tmp.csv", function (err, rows) {

    const startDate = new Date();
    let dates = [];
    let timesteps = unpack(rows, 'Timestep').map(parseFloat);

    timesteps.forEach(e => {
        dates.push(startDate.addDays(e))
    });

    var trace1 = {
        type: "scatter",
        mode: "lines",
        name: "Nombre d'exploitants",
        x: dates,
        y: unpack(rows, "nb_farmers"),
        line: { color: randomColor }
    }

    var trace2 = {
        type: "scatter",
        mode: "lines",
        name: "Installations",
        x: dates,
        y: unpack(rows, "nb_installations"),
        line: { color: randomColor }
    }

    var trace3 = {
        type: "scatter",
        mode: "lines",
        name: "Départs",
        x: dates,
        y: unpack(rows, "nb_retirements"),
        line: { color: randomColor }
    }

    var data1 = [trace1]

    var layout1 = {
        title: "Évolution du nombre d'exploitants",
        xaxis: {
            range: [trace1.x[0], trace1.x[trace1.x.length - 1]],
            type: "date"
        },
        yaxis: {
            autorange: false,
            range: [0, Math.max(...trace1.y.map(parseFloat)) + 10],
            type: "linear"
        }
    }

    var data2 = [trace2, trace3]


    var layout2 = {
        title: "Départs et installations d'exploitants",
        xaxis: {
            range: [trace1.x[0], trace1.x[trace1.x.length - 1]],
            type: "date"
        },
        yaxis: {
            autorange: false,
            range: [0, Math.max(...trace3.y.map(parseFloat)) + 10],
            type: "linear"
        }
    }

    Plotly.newPlot("nb_exploits", data1, layout1)
    Plotly.newPlot("nb_departs_installs", data2, layout2)
})



d3.csv("data/agec_evolutions.csv", function (err, rows) {
    const startDate = new Date();
    let dates = [];
    let timesteps = unpack(rows, '').map(parseFloat);

    timesteps.forEach(e => {
        dates.push(new Date(startDate.setFullYear(startDate.getFullYear() + e)));
    });

    var trace1 = {
        type: "scatter",
        mode: "lines",
        name: "18-30",
        x: dates,
        y: unpack(rows, "18-30"),
        line: { color: randomColor }
    }

    var trace2 = {
        type: "scatter",
        mode: "lines",
        name: "31-50",
        x: dates,
        y: unpack(rows, "31-50"),
        line: { color: randomColor }
    }

    var trace3 = {
        type: "scatter",
        mode: "lines",
        name: "+50",
        x: dates,
        y: unpack(rows, "50+"),
        line: { color: randomColor }
    }

    data = [trace1, trace2, trace3]

    var layout = {
        title: "Évolution du nombre d'exploitants par classe d'âges",
        xaxis: {
            range: [trace1.x[0], trace1.x[trace1.x.length - 1]],
            type: "date"
        },
        yaxis: {
            autorange: false,
            range: [0, Math.max(...trace2.y.map(parseFloat)) + 10],
            type: "linear"
        }
    }

    Plotly.newPlot("ages_classes", data, layout)

});

d3.csv("data/prods_grp.csv", function (err, rows) {

    const startDate = new Date();
    let dates = [];
    let timesteps = unpack(rows, 'TIMESTAMP').map(parseFloat);

    timesteps.forEach(e => {
        dates.push(new Date(startDate.addDays(e)));
    });

    cols = [" Kg []", "cereales Kg []", "cultindustrielles Kg []", "divers Kg []", "fibres Kg []", "fourrage Kg [Bio]", "fourrage Kg []", "fruits Kg [Bio]", "fruits Kg []", "legumes Kg []", "legumineuses Kg [Bio]", "legumineuses Kg []", "oleagineux Kg []", "proteagineux Kg [Bio]", "proteagineux Kg []"];

    traces = []

    cols.forEach(e => {
        traces.push({
            type: "scatter",
            mode: "lines",
            name: e.replace("[", "").replace("]", "").replace("Kg", ""),
            x: dates,
            y: unpack(rows, e),
            line: { color: randomColor }
        });
    })


    var layout = {
        title: "Évolution de la production par cultures",
        xaxis: {
            range: [dates[0], dates[dates.length - 1]],
            type: "date"
        },
        yaxis: {
            autorange: false,
            range: [0, 16000], // magic number FIXIT
            type: "linear"
        }
    }

    Plotly.newPlot("productions", traces, layout)


});