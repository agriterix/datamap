# Agriterix map view

Repository for the Map view of the simulator. Using 
[Openlayers](https://openlayers.org/en/latest/apidoc/module-ol_color.html#~Color), 
[jQuery](https://jquery.com/), [D3.js](https://d3js.org/) and [bootstrap](https://getbootstrap.com/).

This map shows the evolutions after a 50 years [simulation](https://forgemia.inra.fr/agriterix/simulator) on cultural
landscape evolutions. [The data](data/hexagg.geojson) is produced by a 
[processing script](https://forgemia.inra.fr/agriterix/dataprocess).

